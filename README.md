# Down In One

<img src="app/src/main/ic_launcher_musicplayer-web.png" align="left" width="200" hspace="10" vspace="10">
<br/><br/><br/>Down In One is an app I am working on for parties as a project to improve my android development skills. The app is currently part-way through development. Some of the features have not yet been fully implemented including the Settings and playback speed control.<br/>

<br/><br/><br/><br/><br/><br/>

## Screenshots

<div style="display:flex;" >
<img  src="screenshots/screenshot1.jpg" width="20%" >
<img style="margin-left:10px;" src="screenshots/screenshot2.jpg" width="20%" >
<img style="margin-left:10px;" src="screenshots/screenshot3.jpg" width="20%" >
<img style="margin-left:10px;" src="screenshots/screenshot4.jpg" width="20%" >
</div>
