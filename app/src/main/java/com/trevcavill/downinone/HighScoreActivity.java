package com.trevcavill.downinone;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class HighScoreActivity extends AppCompatActivity {

    ArrayList<Score> highScores;
    String bestTimes[] = new String[10];
    String lastDates[] = new String[10];
    String usernames[] = new String[10];
    boolean setTextColor[] = new boolean[10];
    ListView listView;
    ScoreAdapter adapter;
    ArrayList<Score> scores;

    public static final String TAG = "HighScoreActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_list);

        Button btnHighScore = findViewById(R.id.page_heading);
        btnHighScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setSelection(0);
            }
        });

        Button btnMainMenu = findViewById(R.id.menu_back);
        btnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HighScoreActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });


        //Load saved High Scores from shared preferences
        SharedPreferences sharedPrefs = getSharedPreferences("HighScores", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString("HighScores", null);
        Type type = new TypeToken<List<Score>>() {}.getType();
        highScores = gson.fromJson(json, type);

        //Print details of highScores ArrayList to console
        if(highScores != null){
            for(int i = 0; i < highScores.size(); i++){
                Log.v(TAG, "Name" + (i+1) + ": " + highScores.get(i).getName());
                Log.v(TAG, "Time" + (i+1) + ": " + highScores.get(i).getScoreTime());
                Log.v(TAG, "Date" + (i+1) + ": " + highScores.get(i).getScoreDate());
                }
            Log.v(TAG, "ArrayList length: " + highScores.size());
        }

        //Parse info from loaded High Score objects into two ArrayLists
        if(highScores != null){
            for(int i = 0; i < highScores.size(); i++){
                //NB: these two lines of code did not work whereas using a normal array for bestTimes and lastDates instead of ArrayList does...
                //bestTimes.set(i, highScores.get(i).getScoreTime());
                //lastDates.set(i, highScores.get(i).getScoreDate());
                usernames[i] = highScores.get(i).getName();
                bestTimes[i] = highScores.get(i).getScoreTime();
                lastDates[i] = highScores.get(i).getScoreDate();
                setTextColor[i] = highScores.get(i).getIsNewScore();
            }

            for(int j = 10 - (10 - highScores.size()); j < 10; j++){
                usernames[j] = "";
                bestTimes[j] = "";
                lastDates[j] = "";
                setTextColor[j] = false;
            }

        }

       //Create new ArrayList of score objects to be parsed into ScoreAdapter to get displayed on screen
       displayHighScores();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_about:
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(HighScoreActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_about, null);
                mBuilder.setView(mView);
                final AlertDialog aboutDialog = mBuilder.create();
                aboutDialog.show();
                return true;

            case R.id.action_settings:
                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i);
                return true;

            case R.id.action_select_song:
                return true;

            case R.id.action_reset:
                //Reset all high scores
                AlertDialog.Builder rBuilder = new AlertDialog.Builder(HighScoreActivity.this);

                //View rView = getLayoutInflater().inflate(R.layout.dialog_confirm_reset, null);
                //rBuilder.setView(rView);
                //Button btnNo = findViewById(R.id.no_button);
                //Button btnYes = findViewById(R.id.yes_button);

                AlertDialog resetDialog = rBuilder.create();
                resetDialog.setTitle(R.string.reset);
                resetDialog.setMessage(getString(R.string.confirm_reset));
                resetDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        refreshListViews();
                    }
                });

                resetDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.button_no), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        //Close AlertDialog box
                    }
                });

                resetDialog.show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    public void resetHighScores(){

        SharedPreferences sharedPrefs = getSharedPreferences("HighScores", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPrefs.edit();
        Gson gson = new Gson();
        String json1 = sharedPrefs.getString("HighScores", null);
        Type type = new TypeToken<ArrayList<Score>>(){}.getType();
        highScores = gson.fromJson(json1, type);

        if (highScores == null){
            highScores = new ArrayList<>();
        }

        boolean listNotEmpty = true;

        while(listNotEmpty) {
            for(int i = 0; i < highScores.size(); i++){
                highScores.remove(i);
            }
            if(highScores.size() == 0){
                listNotEmpty = false;
            }
        }

        //Save highScores ArrayList to shared preferences
        String json = gson.toJson(highScores);
        prefsEditor.putString("HighScores", json);
        prefsEditor.apply();
    }

    public void displayHighScores(){

        //Create new ArrayList of score objects to be parsed into ScoreAdapter to get displayed on screen
        scores = new ArrayList<>();

        scores.add(new Score("1.  ", bestTimes[0], lastDates[0], usernames[0], setTextColor[0]));
        scores.add(new Score("2.  ", bestTimes[1], lastDates[1], usernames[1], setTextColor[1]));
        scores.add(new Score("3.  ", bestTimes[2], lastDates[2], usernames[2],  setTextColor[2]));
        scores.add(new Score("4.  ", bestTimes[3], lastDates[3], usernames[3],  setTextColor[3]));
        scores.add(new Score("5.  ", bestTimes[4], lastDates[4], usernames[4],  setTextColor[4]));
        scores.add(new Score("6.  ", bestTimes[5], lastDates[5], usernames[5],  setTextColor[5]));
        scores.add(new Score("7.  ", bestTimes[6], lastDates[6], usernames[6],  setTextColor[6]));
        scores.add(new Score("8.  ", bestTimes[7], lastDates[7], usernames[7],  setTextColor[7]));
        scores.add(new Score("9.  ", bestTimes[8], lastDates[8], usernames[8],  setTextColor[8]));
        scores.add(new Score("10. ", bestTimes[9], lastDates[9], usernames[9],  setTextColor[9]));

        adapter = new ScoreAdapter(this, scores);
        listView = findViewById(R.id.list);
        listView.setAdapter(adapter);
    }


    public void setDisplayValues(){

     //Load saved High Scores from shared preferences
        SharedPreferences sharedPrefs = getSharedPreferences("HighScores", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString("HighScores", null);
        Type type = new TypeToken<List<Score>>() {}.getType();
        highScores = gson.fromJson(json, type);

         //Parse info from loaded High Score objects into two ArrayLists
        if(highScores != null){
            for(int i = 0; i < highScores.size(); i++){
                //NB: these two lines of code did not work whereas using a normal array for bestTimes and lastDates instead of ArrayList does...
                //bestTimes.set(i, highScores.get(i).getScoreTime());
                //lastDates.set(i, highScores.get(i).getScoreDate());

                bestTimes[i] = highScores.get(i).getScoreTime();
                lastDates[i] = highScores.get(i).getScoreDate();
                usernames[i] = highScores.get(i).getName();
                setTextColor[i] = highScores.get(i).getIsNewScore();
            }

            for(int j = 10 - (10 - highScores.size()); j < 10; j++){
                bestTimes[j] = "";
                lastDates[j] = "";
                usernames[j] = "";
                setTextColor[j] = false;
            }
        }

    }

    public void refreshListViews() {
        resetHighScores();
        setDisplayValues();

        for(int i = 0; i < 10; i++){
            scores.get(i).setScoreTime(bestTimes[i]);
            scores.get(i).setScoreDate(lastDates[i]);
            scores.get(i).setName(usernames[i]);
            scores.get(i).setIsNewScore(setTextColor[i]);
        }

        adapter.notifyDataSetChanged();
        listView.invalidateViews();
        listView.refreshDrawableState();
    }


}
