package com.trevcavill.downinone;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;


public class Score {
    private String mScoreNum;
    @SerializedName("time")
    private String mTime;
    @SerializedName("date")
    private String mDate;
    @SerializedName("name")
    private String mName;
    @SerializedName("isnew")
    private boolean mIsNewScore;

    public Score(String scoreNum, String time, String date, String name, boolean isNewScore) {
        mScoreNum = scoreNum;
        mTime = time;
        mDate = date;
        mIsNewScore = isNewScore;
        mName = name;
    }

    public Score(String time, String date, String name, boolean isNewScore){
        mTime = time;
        mDate = date;
        mIsNewScore = isNewScore;
        mName = name;
    }


    public String getScoreNum() { return mScoreNum; }
    public String getScoreTime() { return mTime; }
    public String getScoreDate() { return mDate; }
    public String getName() { return mName; }
    public boolean getIsNewScore() { return  mIsNewScore; }



    public void setScoreTime(String time) {
        mTime = time;
    }

    public void setScoreDate(String date) {
        mDate = date;
    }

    public void setName(String name) { mName = name; }

    public void setIsNewScore(boolean isNewScore) { mIsNewScore = isNewScore; }

    static class ScoreComparator implements Comparator<Score> {

        @Override
        public int compare(Score s1, Score s2) {
            if(s1.getScoreTime().equals(s2.getScoreTime())){
                return 0;
            }

            if(s1.getScoreTime() == null){
                return -1;
            }

            if(s2.getScoreTime() == null){
                return 1;
            }

            return s1.getScoreTime().compareTo(s2.getScoreTime());
        }
    }

}
