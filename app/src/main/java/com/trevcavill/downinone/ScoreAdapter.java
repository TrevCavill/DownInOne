package com.trevcavill.downinone;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class ScoreAdapter extends ArrayAdapter<Score> {

    public ScoreAdapter(Activity context, ArrayList<Score> scores) {
        super(context, 0, scores);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        Score currentScore = getItem(position);

        TextView scoreName = listItemView.findViewById(R.id.username);
        scoreName.setText(currentScore.getName());

        TextView scoreNum = listItemView.findViewById(R.id.score_number);
        scoreNum.setText(currentScore.getScoreNum());

        TextView highScore = listItemView.findViewById(R.id.high_score);
        highScore.setText(currentScore.getScoreTime());

        TextView scoreDate = listItemView.findViewById(R.id.score_date);
        scoreDate.setText(currentScore.getScoreDate());

        if(currentScore.getIsNewScore()){
            scoreNum.setTextColor(ContextCompat.getColor(super.getContext(), R.color.blueText));
            highScore.setTextColor(ContextCompat.getColor(super.getContext(), R.color.blueText));
            scoreDate.setTextColor(ContextCompat.getColor(super.getContext(), R.color.blueText));
        }
        else{
            scoreNum.setTextColor(ContextCompat.getColor(super.getContext(), R.color.black));
            highScore.setTextColor(ContextCompat.getColor(super.getContext(), R.color.black));
            scoreDate.setTextColor(ContextCompat.getColor(super.getContext(), R.color.black));
        }


        return listItemView;
    }

}
