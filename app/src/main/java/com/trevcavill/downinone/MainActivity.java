package com.trevcavill.downinone;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    MediaPlayer player;
    SeekBar seekBar;
    ImageButton playBtn;
    TextView tvTimer;
    Handler mainHandler;
    Handler timerHandler = new Handler();
    Runnable runnable;

    boolean songEnded = false;
    boolean autoPlay = false;
    int mProgress;
    int tempScores[];
    String lastUser;
    Uri selectedSong;
    String mySong;
    boolean newSongSelected = false;
    boolean useDefaultSong = false;
    boolean chooseSong = true;

    long startTime = 0L, timeInMilliseconds = 0L, timeSwapBuff = 0L, updateTime = 0L;
    int secs, mins, milliseconds;

    public static final String TAG = "MainActivity";

    ArrayList<Score> highScores;
    private static String time = "00:00:00";
    boolean duplicateTime = false;
    boolean isHighScore = false;

    SharedPreferences sharedPrefs;
    SharedPreferences.Editor prefsEditor;

    Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updateTime = timeSwapBuff + timeInMilliseconds;
            secs = (int)(updateTime / 1000);
            mins = secs / 60;
            secs %= 60;
            milliseconds = (int)(updateTime % 1000);
            milliseconds /= 10;

            time = "" + String.format("%02d", mins) + ":" + String.format("%02d", secs) + ":" + String.format("%02d", milliseconds);

            tvTimer.setText(time);

            timerHandler.postDelayed(this, 0);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = findViewById(R.id.seek_bar);
        mainHandler = new Handler();
        playBtn = findViewById(R.id.play);
        ImageButton ffBtn = findViewById(R.id.ff);
        final ImageButton resetBtn = findViewById(R.id.reset);
        Button scoreBtn = findViewById(R.id.high_scores);
        final Button saveBtn = findViewById(R.id.start);
        tvTimer = findViewById(R.id.timer);

        sharedPrefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        mySong = sharedPrefs.getString("song", "");
        if(!mySong.equals("")){
            selectedSong = Uri.parse(mySong);
            player = MediaPlayer.create(MainActivity.this, selectedSong);
            Toast.makeText(MainActivity.this, "Using saved song." , Toast.LENGTH_SHORT).show();
            chooseSong = false;
            newSongSelected = true;
        }

        if(chooseSong){
            player = MediaPlayer.create(MainActivity.this, R.raw.got_theme);
        }


        if(player != null){
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            seekBar.setMax(player.getDuration());
            player.seekTo(0);
            setPlayButtonImage();
        }


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean input) {
                if(input){
                    if(player != null){
                        player.seekTo(progress);
                    }
                    mProgress = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        playBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(player == null){
                    if(selectedSong != null){
                        player = MediaPlayer.create(MainActivity.this, selectedSong);
                        if(player == null){
                            player = MediaPlayer.create(MainActivity.this, R.raw.got_theme);
                        }
                        else{
                            prepareSong();
                        }
                    }

                    else {
                        player = MediaPlayer.create(MainActivity.this, R.raw.got_theme);
                    }

                    seekBar.setMax(player.getDuration());
                    seekBar.refreshDrawableState();

                    player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    if(mProgress > (player.getDuration() - 30)){
                        player.seekTo(0);
                    }
                    else{
                        player.seekTo(mProgress);
                    }
                }

                //autoPlay = true;

                if(!useDefaultSong && chooseSong){
                  selectSong();
                }

                else{
                    //Used to change the media player when a new song is selected
                    if(!mySong.equals("") && newSongSelected){
                        Toast.makeText(MainActivity.this, "Playing new song.", Toast.LENGTH_SHORT).show();
                        stopPlaying();
                        if(player == null) {
                            Log.v(TAG, "player == null and new MediaPlayer created");
                            player = MediaPlayer.create(MainActivity.this, selectedSong);
                        }

                        prepareSong();
                        seekBar.setMax(player.getDuration());
                        seekBar.refreshDrawableState();
                    }

                    if(player.isPlaying()){
                        player.pause();
                        setPlayButtonImage();
                        timeSwapBuff += timeInMilliseconds;
                        timerHandler.removeCallbacks(updateTimerThread);
                    }

                    else {
                        player.start();
                        playCycle();
                        setPlayButtonImage();
                        startTime = SystemClock.uptimeMillis();
                        timerHandler.postDelayed(updateTimerThread, 0);
                        newSongSelected = false;
                    }



                    player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            //Toast.makeText(MainActivity.this, "The song has ended.", Toast.LENGTH_SHORT).show();
                            songEnded = true;

                            //Restarts song from beginning, this could be an optional feature in settings
                            player.seekTo(0);
                            seekBar.setProgress(0);
                            player.start();
                            playCycle();

                        /*
                        //Pauses timer and stops music
                        setPlayButtonImage();
                        stopPlaying();
                        timeSwapBuff += timeInMilliseconds;
                        timerHandler.removeCallbacks(updateTimerThread);
                         */
                        }
                    });
                }
            }
        });

        //Save new High Score so that it will be stored and shown on the HighScoreActivity screen
        saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /*
                Reassigns time variable so that if user switches back to MainActivity from HighScoreActivity the
                time variable is set back to zero rather than being the same as the last score.
                */
                checkTime();
                //Allows user to save a new High Score if the time is greater than 0 seconds
                if(!time.equals("00:00:00")){
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.dialog_enter_username, null);
                    final EditText mLogin = mView.findViewById(R.id.etUsername);
                    Button btnSubmit = mView.findViewById(R.id.submit_button);
                    Button btnCancel = mView.findViewById(R.id.cancel_button);

                    //Load existing highScores ArrayList from shared preferences
                    SharedPreferences sharedPrefs = getSharedPreferences("HighScores", MODE_PRIVATE);
                    Gson gson = new Gson();
                    String json1 = sharedPrefs.getString("HighScores", null);
                    Type type = new TypeToken<ArrayList<Score>>(){}.getType();
                    highScores = gson.fromJson(json1, type);

                    //Get the most recently used username
                    if(highScores != null){
                        for(int i = 0; i < highScores.size(); i++){
                            if(highScores.get(i).getIsNewScore()){
                                lastUser = highScores.get(i).getName();
                            }
                        }
                    }

                    //Set the username field to be the last username by default
                    mLogin.setText(lastUser);
                    //Auto-selects the username so that it's easy to change if desired
                    mLogin.setSelectAllOnFocus(true);

                    mBuilder.setView(mView);
                    final AlertDialog loginDialog = mBuilder.create();
                    loginDialog.show();

                    btnSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!mLogin.getText().toString().isEmpty()){

                                String username = mLogin.getText().toString();
                                loginDialog.dismiss();

                                //Get today's date, format it and parse it to String variable
                                long millis = System.currentTimeMillis();
                                java.sql.Date date = new java.sql.Date(millis);
                                DateFormat df = new SimpleDateFormat("dd/MM/yy");
                                String currentDate = df.format(date);

                                //Prints new score details to the console
                                Log.v(TAG, "New Time: " + time);
                                Log.v(TAG, "Current Date: " + currentDate);
                                Log.v(TAG, "Username: " + username);

                                //Load existing highScores ArrayList from shared preferences
                                SharedPreferences sharedPrefs = getSharedPreferences("HighScores", MODE_PRIVATE);
                                SharedPreferences.Editor prefsEditor = sharedPrefs.edit();
                                Gson gson = new Gson();
                                String json1 = sharedPrefs.getString("HighScores", null);
                                Type type = new TypeToken<ArrayList<Score>>(){}.getType();
                                highScores = gson.fromJson(json1, type);

                                if (highScores == null){
                                    highScores = new ArrayList<>();
                                }

                                for(int i = 0; i < highScores.size(); i++){
                                    //Set all values to false so that only new score is true
                                    highScores.get(i).setIsNewScore(false);
                                    //Check for existing time of the same value and username to prevent duplicate scores
                                    if(highScores.get(i).getScoreTime().equals(time) && highScores.get(i).getName().equals(username)){
                                        Toast.makeText(MainActivity.this, "A duplicate time already exists for this user.", Toast.LENGTH_SHORT).show();
                                        duplicateTime = true;
                                    }

                                    //Checks to make sure that the new time is lower than at least one of the existing times
                                    if(highScores.size() == 10){

                                        tempScores = new int[10];

                                        for(int j = 0; j < 10; j++){
                                            tempScores[j] = Integer.parseInt(highScores.get(i).getScoreTime().replace(":", ""));
                                            if(tempScores[j] > (Integer.parseInt(time.replace(":", "")))) {
                                                isHighScore = true;
                                            }
                                        }
                                    }
                                }

                                if(!duplicateTime){
                                    //If the new score is better than all existing scores, print toast to say 'New top score!'
                                    if(highScores.size() > 0 && Integer.parseInt(highScores.get(0).getScoreTime().replace(":", "")) > (Integer.parseInt(time.replace(":", "")))){
                                        if(highScores.size() < 10){
                                            highScores.add(new Score(time, currentDate, username, true));
                                        }

                                        else {
                                            highScores.get(9).setScoreTime(time);
                                            highScores.get(9).setScoreDate(currentDate);
                                            highScores.get(9).setName(username);
                                            highScores.get(9).setIsNewScore(true);
                                        }
                                        Toast.makeText(MainActivity.this, "New top score!", Toast.LENGTH_SHORT).show();
                                    }
                                    //Add new score to the ArrayList if it is better than any of the top ten existing scores
                                    else if(highScores.size() < 10){
                                        highScores.add(new Score(time, currentDate, username, true));
                                        Toast.makeText(MainActivity.this, "High Score saved.", Toast.LENGTH_SHORT).show();
                                    }

                                    else if(isHighScore) {
                                        highScores.get(9).setScoreTime(time);
                                        highScores.get(9).setScoreDate(currentDate);
                                        highScores.get(9).setName(username);
                                        highScores.get(9).setIsNewScore(true);
                                        Toast.makeText(MainActivity.this, "High Score saved.", Toast.LENGTH_SHORT).show();
                                    }

                                    else {
                                        Toast.makeText(MainActivity.this, "No new High Score, better luck next time!", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                //Compare times between High Score objects and sort them
                                Collections.sort(highScores, new Score.ScoreComparator());

                                //Save highScores ArrayList to shared preferences
                                String json = gson.toJson(highScores);
                                prefsEditor.putString("HighScores", json);
                                prefsEditor.apply();
                                isHighScore = false;
                            }

                            else{
                                Toast.makeText(MainActivity.this, "Please enter a valid name and try again.", Toast.LENGTH_SHORT).show();
                            }
                        }

                    });

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loginDialog.dismiss();
                        }
                    });
                }

                else {
                    Toast.makeText(MainActivity.this, "Unless you're a God, I don't think you consumed your beverage in 0 seconds.", Toast.LENGTH_LONG).show();
                }
                duplicateTime = false;
            }
        });

        //NB: This button does not currently function properly...
        ffBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                /*
                if((player.getDuration() / 12) > (player.getCurrentPosition() + (player.getDuration() / 12))){
                    seekBar.setProgress(player.getCurrentPosition() + (player.getDuration() / 12));
                    player.seekTo(player.getCurrentPosition() + (player.getDuration() / 12));
                }
                else {
                    seekBar.setProgress(player.getDuration());
                    player.seekTo(player.getDuration());
                }

                */
            }
        });


        resetBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                stopPlaying();

                if(selectedSong != null){
                    player = MediaPlayer.create(MainActivity.this, selectedSong);
                }

                else {
                    player = MediaPlayer.create(MainActivity.this, R.raw.got_theme);
                }

                seekBar.setMax(player.getDuration());
                seekBar.setProgress(0);

                setPlayButtonImage();
                tvTimer.setText(R.string.timer);
                timerHandler.removeCallbacks(updateTimerThread);
                startTime = 0L;
                timeInMilliseconds = 0L;
                timeSwapBuff = 0L;
                updateTime = 0L;

            }
        });

        scoreBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, HighScoreActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_about:
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.dialog_about, null);
                mBuilder.setView(mView);
                final AlertDialog aboutDialog = mBuilder.create();
                aboutDialog.show();
                return true;

            case R.id.action_settings:
                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i);
                return true;

            case R.id.action_select_song:
                Intent audiofile_chooser_intent = new Intent();
                audiofile_chooser_intent.setAction(Intent.ACTION_GET_CONTENT);
                audiofile_chooser_intent.setType("audio/*");
                startActivityForResult(audiofile_chooser_intent, 1);
                return true;

            case R.id.action_use_default:
                stopPlaying();
                mySong = "";
                selectedSong = null;
                prefsEditor = sharedPrefs.edit();
                prefsEditor.putString("song", mySong);
                prefsEditor.apply();
                player = MediaPlayer.create(MainActivity.this, R.raw.got_theme);
                return true;

            case R.id.action_test:
                Log.v(TAG, "Song uri: " + selectedSong);
                Log.v(TAG, "String of uri: " + mySong);
                return true;

            //Reset all high scores
            case R.id.action_reset:
                AlertDialog.Builder rBuilder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog resetDialog = rBuilder.create();
                resetDialog.setTitle(R.string.reset);
                resetDialog.setMessage(getString(R.string.confirm_reset));
                resetDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        resetHighScores();
                    }
                });

                resetDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.button_no), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        //Close AlertDialog box
                    }
                });

                resetDialog.show();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void selectSong(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        AlertDialog songDialog = builder.create();
        songDialog.setTitle(R.string.select_song);
        songDialog.setMessage(getString(R.string.choose_song));

        songDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_browse), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent audiofile_chooser_intent = new Intent();
                audiofile_chooser_intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                audiofile_chooser_intent.setType("audio/*");
                audiofile_chooser_intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(audiofile_chooser_intent, 1);
            }
        });

        songDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.button_default), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                chooseSong = false;
                newSongSelected = false;
                mySong = "";
                selectedSong = null;
                prefsEditor = sharedPrefs.edit();
                prefsEditor.putString("song", mySong);
                prefsEditor.apply();
                player = MediaPlayer.create(MainActivity.this, R.raw.got_theme);
                Toast.makeText(MainActivity.this, "Using default song.", Toast.LENGTH_SHORT).show();
            }
        });

        songDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){

        if(requestCode == 1){

            if(resultCode == RESULT_OK){
                selectedSong = data.getData();
                newSongSelected = true;
                chooseSong = false;

                if(selectedSong != null){
                    mySong = selectedSong.toString();
                }
                prefsEditor = sharedPrefs.edit();
                prefsEditor.putString("song", mySong);
                prefsEditor.apply();

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void prepareSong() {
        try {
            player.setDataSource(new FileInputStream(new File(selectedSong.getPath())).getFD());
            player.prepare();
            //player.setDataSource(context, uri);
            //player.prepare();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playCycle() {
        seekBar.setProgress(player.getCurrentPosition());
        autoPlay = true;
        if(player.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    playCycle();
                }
            };
            mainHandler.postDelayed(runnable, 1000);
        }

        //else if(songEnded){
        //    seekBar.setProgress(0);
        //}
    }


    @Override
    protected void onResume() {
        super.onResume();
       // if(autoPlay){
            //Can be used to auto-resume song when user leaves app and returns (settings toggle?)
            /*
            player.start();
            playCycle();
            setPlayButtonImage();
            startTime = SystemClock.uptimeMillis();
            timerHandler.postDelayed(updateTimerThread, 0);
            //songEnded = false;
            */
        //}
    }


    @Override
    protected void onPause() {
        super.onPause();
        player.pause();
        autoPlay = false;
        setPlayButtonImage();
        timeSwapBuff += timeInMilliseconds;
        timerHandler.removeCallbacks(updateTimerThread);
    }


    private void stopPlaying(){
        if(player != null){
            player.stop();
            player.reset();
            player.release();
            player = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
        mainHandler.removeCallbacks(runnable);
    }

    private void setPlayButtonImage(){
        if(player.isPlaying()){
            playBtn.setImageResource(android.R.drawable.ic_media_pause);
        }

        else {
            playBtn.setImageResource(android.R.drawable.ic_media_play);
        }
    }

    public void resetHighScores(){

        SharedPreferences sharedPrefs = getSharedPreferences("HighScores", MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPrefs.edit();
        Gson gson = new Gson();
        String json1 = sharedPrefs.getString("HighScores", null);
        Type type = new TypeToken<ArrayList<Score>>(){}.getType();
        highScores = gson.fromJson(json1, type);

        if (highScores == null){
            highScores = new ArrayList<>();
        }

        boolean listNotEmpty = true;

        while(listNotEmpty) {
            for(int i = 0; i < highScores.size(); i++){
                highScores.remove(i);
            }
            if(highScores.size() == 0){
                listNotEmpty = false;
            }
        }

        //Save highScores ArrayList to shared preferences
        String json = gson.toJson(highScores);
        prefsEditor.putString("HighScores", json);
        prefsEditor.apply();
    }

    public void checkTime(){
        if(tvTimer.getText().equals("00:00:00")){
            time = "00:00:00";
        }
    }
}
